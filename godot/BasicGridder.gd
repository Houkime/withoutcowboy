extends Node2D

class_name BasicGridder

const BUBBLETIME=2

var CurrentLook = "Cowboy"
var visual
var grid
var ii
var jj
var InvisibleButtonLayer
var speaksnow=false
onready var tw=Tween.new()

var ButtInfo=Array()

var offsets=[
Vector2(0,-120),
Vector2(120,-120),
Vector2(240,-120),
Vector2(360,-120)
]

var spawns=Array()
var enabled=true

func spawn_buttons():
	for i in range(ButtInfo.size()):
		var b = ButtonSpawn.new(ButtInfo[i].action,ButtInfo[i].texture,offsets[i])
		add_child(b)
		spawns.append(b)
	pass


func _ready():
	grid=$".."
	visual=grid.VisualDict[CurrentLook].instance()
	add_child(visual)
	add_child(tw)
	InvisibleButtonLayer=TextureButton.new()
	add_child(InvisibleButtonLayer)
	InvisibleButtonLayer.rect_size=Vector2(200,200)
	InvisibleButtonLayer.connect("button_down",self,"on_click")
	print ("I am first?")

func clearspawns():
	for s in spawns:
		s.queue_free()
	spawns.clear()
	enabled=true

func on_click():
	print ("clicked")
	if (!grid.scene_mode):
		if (enabled):
			print ("button pressed")
			enabled=false
			spawn_buttons()
		else:
			clearspawns()
	else:
		if (speaksnow):
			speaksnow=false
			grid.advance()
	

func create_blob(message, scenic=false):
	var n=Node2D.new()
	var bt=BUBBLETIME
	if (scenic):
		bt=1000
		speaksnow=true
	n.z_index=8
	add_child(n)
	n.add_child(Bubble.new(message,bt))
	if (scenic):
		grid.scenic_bubbles.append(n)