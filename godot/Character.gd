extends "res://BasicGridder.gd"


var nexti
var nextj
var moving
var tweentime
var special=false


class_name Character

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.


func _ready():
	tweentime=Timer.new()
	tweentime.one_shot=true
	add_child(tweentime)
	grid.TIMER.connect("timeout",self,"tic")
	pass # Replace with function body.

func tic():
	if (!special):
		var ft=grid.get_free_tile()
		queue_move(ft.i,ft.j)
		move()
		

func queue_move(i,j):
	nexti=i
	nextj=j
	
func move(a="nop"):

	var time=$"../Timer".wait_time*0.9
	var nextpos
	nextpos=grid.Tiles[nexti][nextj].position
	#tw.connect("tween_completed",self,a,[],CONNECT_ONESHOT)
	tweentime.wait_time=time
	tweentime.connect("timeout",self,a,[],CONNECT_ONESHOT)
	tw.interpolate_property(self,"position",position,nextpos,time,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tw.start()
	tweentime.start()
	ii=nexti
	jj=nextj
	#print("connected "+String(tw.is_connected("tween_started",self,"nop")))

func move_away():
	special=true
	grid.Chars.remove(grid.Chars.find(self))
	move_special(Vector2(-1000,0),"free")

func move_special(vector,a="nop"):
	var time=$"../Timer".wait_time*0.9
	tweentime.wait_time=time
	tweentime.connect("timeout",self,a,[],CONNECT_ONESHOT)
	tw.interpolate_property(self,"position",position,vector,time,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tw.start()
	tweentime.start()

func nop():
	print("nop triggered")
	pass

func speak():
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
