extends "res://Building.gd"

class_name Toilet

const MILKANITIME=0.3
const COST=80

var milktw
var has_girl=false

onready var toilsprite=$"Building/Build"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func stop_milking():
	if (has_girl):
		has_girl=false
		milktw.stop_all()
		grid.add_char(Girl)
		toilsprite.scale=Vector2(1,1)
	else:
		create_blob("no girl inside :-<")

func start_milking():
	milktw.start()
	pass

func _ready():
	grid.toilets.append(self)
	grid.TIMER.connect("timeout",self,"tic")
	milktw=Tween.new()
	var organ=toilsprite
	milktw.interpolate_property(organ,"scale",organ.scale, Vector2(1,0.5),MILKANITIME,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	add_child(milktw)
	milktw.repeat=true
	
	pass # Replace with function body.
	
	
func _init(bclass=null):
	CurrentLook="Toilet"
	ButtInfo=[ 
		{
		texture="res://sprites/GirlHair.0001.png",
		action="stop_milking"
		}
		]
	
func tic():
	if (has_girl):
		grid.Economical.milk+=1
		spit_milk(toilsprite.position,toilsprite.scale/3)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
