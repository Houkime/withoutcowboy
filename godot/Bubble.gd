extends PanelContainer

class_name Bubble

const SCALE=Vector2(3,3) 
const MAXWIDTH=100
var offset=Vector2(120,-60)
var label
var message
var lifetime
var anitime=1
var timer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _init(text,time):
	message=text
	lifetime=time

func _ready():
	var tween1=Tween.new()
	var tween2=Tween.new()
	add_child(tween1)
	add_child(tween2)
	rect_position+=offset
	if (rect_global_position.x>500):
		rect_position+=Vector2(-120,0)
	rect_scale=SCALE
	label= Label.new() 
	add_child(label)
	label.text=message
	timer= Timer.new() 
	add_child(timer)
	timer.wait_time=lifetime	
	timer.connect("timeout",$"..","free")
	timer.start()
	tween1.interpolate_property(self,"rect_position",rect_position-Vector2(0,-60),rect_position,anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween1.start()
	tween2.interpolate_property(self,"modulate",Color(1,1,1,0),Color(1,1,1,1),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween2.start()

	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
