extends "res://Character.gd"

const OBSTRUCTIONCOST=100
const TIMEBONUS=5

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
class_name OtherCowboy

func _init():
	CurrentLook="OtherCowboy"
	ButtInfo=[ {
		texture="res://sprites/CowboyBody.0001.png",
		action="obstruct"
		}
		]

func obstruct():
	if (grid.Economical.milk>=OBSTRUCTIONCOST):
		grid.Economical.milk-=OBSTRUCTIONCOST
		move_away()
		create_blob("COUNT HIM DEAD!")
		grid.Economical.days+=TIMEBONUS
	else:
		create_blob(String(OBSTRUCTIONCOST)+" milk - no Johny")
	
func _ready():

	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
