extends Node2D

const GIRLTIME=20
const HAYTIME=5
const COWBOYTIME=20
const HAYBONUS=5

enum modes{
	GIRL,
	HAY,
	COWBOY
}

var wait_time=0
var elapsed=0
var action="nop"

func nop():
	print("nop on expedition")

onready var timer=$"../Timer"
onready var grid=$".."

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	timer.connect("timeout",self,"on_tick")
	$PanelSizer.scale.x=0
	pass # Replace with function body.

func on_tick():
	if (visible):
		elapsed+=1
		$PanelSizer.scale.x=float(elapsed)/float(wait_time)
		if (elapsed>=wait_time):
			visible=false
			wait_time=0
			elapsed=0
			grid.add_char(FakeCowboy)
			call(action)
			$PanelSizer.scale.x=0
			$Sprite.texture=null
		
	pass

func reset():
	visible=false
	$PanelSizer.scale.x=0
	action="nop"
	wait_time=0
	elapsed=0

func start(mode):
	if (mode==modes.HAY):
		action="hay"
		wait_time=HAYTIME
		$Sprite.texture=load("res://sprites/Haystack.0001.png")
	if (mode==modes.GIRL):
		action="girl"
		wait_time=GIRLTIME
		$Sprite.texture=load("res://sprites/GirlHair.0001.png")
	if (mode==modes.COWBOY):
		action="cowboy"
		wait_time=COWBOYTIME
		$Sprite.texture=load("res://sprites/CowboyHat.0001.png")
	visible=true

func hay():
	print("hay made!!")
	grid.Economical.hay+=HAYBONUS
	pass	
func girl():
	grid.add_char(Girl)
	grid.Economical.girls+=1
	pass	
func cowboy():
	grid.add_char(OtherCowboy)
	pass		
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
