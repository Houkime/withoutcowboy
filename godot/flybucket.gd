extends Node2D

class_name FlyBucket

var TEXTURE=load("res://sprites/Bucket_full.0001.png")
var offset=Vector2(120,-60)
var sprite
var anitime=1
var timer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.

func _init(o,s):
	offset=o
	scale=s

func _ready():
	var tween1=Tween.new()
	var tween2=Tween.new()
	add_child(tween1)
	add_child(tween2)
	z_index=7
	position+=offset
	sprite= Sprite.new() 
	add_child(sprite)
	sprite.texture=TEXTURE
	timer= Timer.new() 
	add_child(timer)
	timer.wait_time=anitime	
	timer.connect("timeout",self,"free")
	timer.start()
	tween1.interpolate_property(self,"position",position,position+Vector2(0,-60),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween1.start()
	tween2.interpolate_property(self,"modulate",Color(1,1,1,1),Color(1,1,1,0),anitime,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	tween2.start()

	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
