extends Node2D

const FOODNEEDED=10

var Tileclass=load("res://Tile.tscn")
var CornerSprite=load("res://CornerSprite.tscn")
var FenceSprite=load("res://FenceSprite.tscn")
var VisualDict={
	CowA=load("res://CowA.tscn"),
	CowB=load("res://CowB.tscn"),
	Cowboy=load("res://Cowboy.tscn"),
	OtherCowboy=load("res://OtherCowboy.tscn"),
	Woman=load("res://Woman.tscn"),
	RichGuy=load("res://RichGuy.tscn"),
	FrogAdvocate=load("res://FrogAdvocate.tscn"),
	FakeCowboy=load("res://FakeCowboy.tscn"),
	Toilet=load("res://Toilet.tscn"),
	Bucket=load("res://Bucket.tscn"),
	Cross=load("res://Cross.tscn")
	
	}

const TILESIZE=80
const SOURCESIZE=200
var TILESCALE=float(TILESIZE)/float(SOURCESIZE)
var VTILESCALE=Vector2(TILESCALE,TILESCALE)
var TILESX=9
var TILESY=6
var Tiles=Array()
var Chars=Array()
const EconomicalFallback={
	hay=20,
	milk=0,
	girls=0,
	hats=1,
	fakes=0,
	days=15,
	hours=0
	}
var Economical
var buildtarget
var buckets=Array()
var toilets=Array()
var cows=Array()
var guest
var scenic_bubbles=Array()
var scene_mode=false
var scene_state

onready var TIMER=$"Timer"
onready var GuestPlace=$"VisitorPlace"

func buildmode_enable(bt):
	buildtarget=bt
	for a in Tiles:
		for b in a:
			b.activate()
			
func buildmode_disable():
	for a in Tiles:
		for b in a:
			b.deactivate()

func spawn_building(i,j,cl):
	buildmode_disable()
	var ch=spawn_entity(i,j,cl)
	Tiles[i][j].occupied=true
	ch.ii=i
	ch.jj=j
	return ch

func erase_building(i,j,ch):
	ch.queue_free()
	Tiles[i][j].occupied=false
	
func exchange_building (i,j,ch,bc):
	erase_building(i,j,ch)
	spawn_building(i,j,bc)

func spawn_entity(i,j,cl):
	var ch=cl.new()
	ch.position=Tiles[i][j].position
	ch.scale=VTILESCALE
	add_child(ch)
	return ch

func remove_char(ch):
	Chars.remove(Chars.find(ch))
	ch.queue_free()

func exchange_char(i,j,ch,cl):
	Chars.remove(Chars.find(ch))
	ch.queue_free()
	spawn_char(i,j,cl)

func spawn_char(i,j,cl):
	var ch=cl.new()
	ch.position=Tiles[i][j].position
	ch.scale=VTILESCALE
	add_child(ch)
	Chars.append(ch)
	ch.ii=i
	ch.jj=j

func add_char(cl):
	var ft=get_free_tile()
	spawn_char(ft.i,ft.j,cl)
	pass

func get_free_tile():
	var freetiles=Array()
	for a in Tiles:
		for t in a:
			if (!t.occupied):
				freetiles.append(t)
	var result
	result=freetiles[randi()%freetiles.size()]
	return result
	

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func add_row_fence(origin,v):
	if (!v):
		for i in range(1,TILESX):
			var fence=FenceSprite.instance()
			fence.position=origin+Vector2(i*TILESIZE,0)
			fence.scale=VTILESCALE
			add_child(fence)
	else:
		for i in range(1,TILESY):
			var fence=FenceSprite.instance()
			fence.rotation_degrees=90
			fence.position=origin+Vector2(0,i*TILESIZE)
			fence.scale=VTILESCALE
			add_child(fence)

func add_fence():
	add_row_fence(Vector2(0,0),false)
	add_row_fence(Vector2(0,0),true)
	add_row_fence(Vector2(0,TILESY*TILESIZE),false)
	add_row_fence(Vector2(TILESX*TILESIZE,0),true)
	
	var cornersprite1=CornerSprite.instance()
	cornersprite1.position=Vector2(0,0)
	cornersprite1.scale=VTILESCALE
	add_child(cornersprite1)
	
	var cornersprite2=CornerSprite.instance()
	cornersprite2.position=Vector2(TILESX*TILESIZE,0)
	cornersprite2.scale=VTILESCALE
	cornersprite2.flip_h=true
	add_child(cornersprite2)
	
	var cornersprite3=CornerSprite.instance()
	cornersprite3.position=Vector2(0,TILESY*TILESIZE)
	cornersprite3.scale=VTILESCALE
	cornersprite3.flip_v=true
	add_child(cornersprite3)
	
	var cornersprite4=CornerSprite.instance()
	cornersprite4.position=Vector2(TILESX*TILESIZE,TILESY*TILESIZE)
	cornersprite4.scale=VTILESCALE
	cornersprite4.flip_h=true
	cornersprite4.flip_v=true
	add_child(cornersprite4)

func init_tiles():
	
	for i in range (TILESX):
		var yarray=Array()
			
		for j in range (TILESY):
			var tile=Tileclass.instance()
			add_child(tile)
			tile.position=Vector2(i*TILESIZE,j*TILESIZE)
			tile.scale=Vector2(TILESCALE,TILESCALE)
			tile.i=i
			tile.j=j
			yarray.append(tile)
			
		Tiles.append(yarray)

func spawn_cows(number):
	for z in range(number):
		add_char(Cow)

func call_guest(cl):
	guest=spawn_entity(0,0,cl)
	guest.position=GuestPlace.position
	guest.special=true
func remove_guest():
	if (guest!=null):
		guest.queue_free()
		guest=null

func _ready():
	
	add_fence()
	init_tiles()
	set_up()
	
	pass 

func set_up():
	scene_mode=false
	Economical=EconomicalFallback.duplicate()
	spawn_cows(3)
	spawn_building(4,4,Bucket)
	start_scenario("welcome_scenario")
	

func prepare_gameend():
	TIMER.stop()
	scene_mode=true
	$Resetter.visible=true

func gameover_time():
	prepare_gameend()
	$"Gameender/Slavery".visible=true

func gameover_starvation():
	prepare_gameend()
	$"Gameender/Starvation".visible=true

func win():
	prepare_gameend()
	$"Gameender/Win".visible=true


func advance():
	print("advancing")
	scene_state=scene_state.resume()
	pass
func skip_scene():
	$Skipper.visible=false
	while (!scene_state==null):
		clear_scenic_blobs()
		advance()
func reset():
	for s in [$Scenario1,$Scenario2]:
		s.visible=false
	$Resetter.visible=false
	$Expedition.reset()
	$Gameender.hide_all()
	TIMER.stop()
	clear_scenic_blobs()
	Economical.clear()
	free_n_clear(Chars)
	cows.clear()
	free_n_clear(buckets)
	free_n_clear(toilets)
	for c in get_children():
		if c.is_class("Cross"):
			c.queue_free()
	for a in Tiles:
		for t in a:
			t.occupied=false
	buildmode_disable()
	set_up()

func free_n_clear(array):
	for e in array:
		e.queue_free()
	array.clear()

func clear_scenic_blobs():
	for b in scenic_bubbles:
		if (b is Node2D):
			b.visible=false
			b.get_parent().speaksnow=false
			b.queue_free()
	scenic_bubbles.clear()
	print ("array size is "+ str(scenic_bubbles.size()))

func welcome_scenario():
	
	TIMER.stop()
	scene_mode=true
	call_guest(Cowboy)
	
	guest.create_blob("You can't click me!",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("I am Johny Spherical!",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("wait... Was that a click?",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("Anyway! I'm off to adventures and blood!",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("Because I totally don't care about my profession or cows!",true)
	yield()
	
	remove_guest()
	
	cowspeak("and what now?")
	yield()
	cowspeak("winter around")
	yield()
	cowspeak("no grass")
	yield()
	cowspeak("only hay")
	yield()
	cowspeak("2 days' worth \n to be precise")
	yield()
	cowspeak("and we're cows")
	yield()
	cowspeak("large brains")
	yield()
	cowspeak("without legal status")
	yield()
	cowspeak("hmm")
	yield()
	cowspeak("JURIS!!!!")
	yield()
	
	call_guest(FrogAdvocate)
	
	clear_scenic_blobs()
	guest.create_blob("Oh,hello",true)
	yield()
	
	cowspeak("OMG!!!!")
	yield()
	
	cowspeak("TALKING FROG!!!!")
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("can we get to business?",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("I'm a very experienced professional",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("Just from a glance...",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("You're in trouble",true)
	yield()
	
	cowspeak("OMG!!!!")
	yield()
	
	cowspeak("How did you \n know that?")
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("You're a property",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("a property of that loud guy",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("and winter is coming",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("hmm",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("you know what?",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("it's probably good that he is not here",true)
	yield()
	
	cowspeak("how so?")
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("that guy is very simple",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("with this HAT and some paint you'll be just like him",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("Here, take it",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("you can get yourself some hay with it",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("Unfortunately you can't kill the original",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("It is western and he is a dense hero",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("he is immortal by genre",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("but you can obstruct him from eventually returning back",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("Armed people here will do anything for milk",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("they like organic food and shooting people",true)
	yield()
	
	cowspeak("hmm")
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("later, I will need your help",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("my current client is powerful, but nervous",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("we can both go to his good side",true)
	yield()
	
	$Scenario1.visible=true
	remove_guest()
	clear_scenic_blobs()
	scene_mode=false
	$Skipper.visible=false
	TIMER.start()

func good_side():
	TIMER.stop()
	scene_mode=true
	call_guest(RichGuy)
	
	clear_scenic_blobs()
	guest.create_blob("oh,this is just perfect",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("my fixer knows the best places",true)
	yield()
	
	$NSFW.visible=true
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("fantastically refreshing when they are three",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("... hope no need for hush money",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("...",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("ok, now to business",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("we need tons of money for our project",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("and by money, i mean milk",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("it is a federal project, but unfortunately...",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("..now we need to have a partial private funding to \n pump money from the budget",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("and the Scheriff won't let us do too much tricks",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("you cows and me are now best friends, right?",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("if the project succeeds, we will talk about Scheriff position",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("more then free, you will be the LAW",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("and my best friends of course",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("bye",true)
	yield()
	
	$Scenario2.visible=true
		
	remove_guest()
	clear_scenic_blobs()
	scene_mode=false
	$Skipper.visible=false
	TIMER.start()
	
	
	pass
func finish():
	TIMER.stop()
	scene_mode=true
	call_guest(RichGuy)
	
	clear_scenic_blobs()
	guest.create_blob("wow",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("didn't expect that",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("now our project will succeed",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("with 50/50 share between me and Frog",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("I'll try and pull some strings to make you sheriffs",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("now that we have so much money, i'm unstoppable",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("thank you dear friends",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("I'll indeed enjoy you as sheriffs",true)
	yield()
	
	cowspeak("hOORAY!!")
	yield()
	
	remove_guest()
	
	cowspeak("hOOoooRAY!")
	yield()
	cowspeak("hooray...??")
	yield()
	
	call_guest(FrogAdvocate)
	
	clear_scenic_blobs()
	guest.create_blob("very unpleasant guy, right?",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("now, scheriffs, we'll talk how we become the real power",true)
	yield()
	
	clear_scenic_blobs()
	guest.create_blob("Power without humans",true)
	yield()
	$Skipper.visible=false
	win()
	
func start_scenario(name):
	scene_state=null
	$Skipper.visible=true
	scene_state=call(name)

func cowspeak(message):
	randomize()
	var cow
	clear_scenic_blobs()
	cow=cows[randi()%cows.size()]
	cow.create_blob(message,true)

func _on_Timer_timeout():
	print ("tick")
	Economical.hours+=1
	if (Economical.hours>=24):
		if (Economical.days<1):
			gameover_time()
		else:
			if (Economical.hay>=FOODNEEDED):
				Economical.hay-=FOODNEEDED
				Economical.days-=1
				Economical.hours=0
			else:
				gameover_starvation()
