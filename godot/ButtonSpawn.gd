extends Node2D

class_name ButtonSpawn

var rel_scale=Vector2(0.5,0.5)

var action
var texture
var offset
var tween1
var tween2
var pc
var tb


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.

func _init(a,t,o):
	action=a
	texture=t
	offset=o

func _ready():
	position+=offset
	scale=rel_scale
	z_index=8
	pc=PanelContainer.new()
	add_child(pc)
	tb=TextureButton.new()
	pc.add_child(tb)
	tb.texture_normal=load(texture)
	tb.connect("button_down",$"..",action)
	tb.connect("button_down",$"..","clearspawns")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
