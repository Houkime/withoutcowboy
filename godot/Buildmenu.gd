extends TextureButton

var grid

var ButtInfo=[ {
	texture="res://sprites/Toilet.0001.png",
	action="make_toilet"
	},
	{
	texture="res://sprites/Bucket_empty.0001.png",
	action="make_bucket"
	}
	
]

var offsets=[
Vector2(0,-120),
Vector2(120,-120)
]
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var spawns=Array()
var enabled=true

func spawn_buttons():
	for i in range(ButtInfo.size()):
		var b = ButtonSpawn.new(ButtInfo[i].action,ButtInfo[i].texture,offsets[i])
		add_child(b)
		spawns.append(b)
	pass


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func clearspawns():
	for s in spawns:
		s.queue_free()
	spawns.clear()
	enabled=true

func make_toilet():
	print ("trying to make a toilet")
	grid=$"../../../GridOrigin"
	grid.buildmode_enable(Toilet)
	
	
func make_bucket():
	print ("trying to make a bucket")
	grid=$"../../../GridOrigin"
	grid.buildmode_enable(Bucket)

func _on_TextureButton_button_down():
	grid=$"../../../GridOrigin"
	if (enabled&&(!grid.scene_mode)):
		print ("button pressed")
		enabled=false
		spawn_buttons()
	else:
		clearspawns()
