extends "res://Building.gd"

class_name Bucket

var has_cow=false
onready var bucsprite=$"Building/Build"

const COST=10

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	grid.buckets.append(self)
	grid.TIMER.connect("timeout",self,"tic")
	InvisibleButtonLayer.visible=false
	pass # Replace with function body.
func _init(bclass=null):
	CurrentLook="Bucket"
func tic():
	if (has_cow):
		grid.Economical.milk+=1
		spit_milk(bucsprite.position,bucsprite.scale)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
