extends "res://Character.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
class_name FakeCowboy

const HAYPRICE=6

onready var expedition=$"../Expedition"

func _init():
	CurrentLook="FakeCowboy"
	ButtInfo=[ {
		texture="res://sprites/Haystack.0001.png",
		action="get_hay"
		},
		{
		texture="res://sprites/CowBody2.0001.png",
		action="become_cow"
		},
		{
		texture="res://sprites/CowboyHat.0001.png",
		action="get_cowboy"
		},
		{
		texture="res://sprites/GirlHair.0001.png",
		action="get_girl"
		}
		]

func get_hay():
	if (grid.Economical.milk>=HAYPRICE):
		grid.Economical.milk-=HAYPRICE
		move_away()
		expedition.start(expedition.modes.HAY)
	else:
		create_blob("need " + String(HAYPRICE)+ " milk")
	pass

func get_girl():
	move_away()
	expedition.start(expedition.modes.GIRL)
	pass

func get_cowboy():
	move_away()
	expedition.start(expedition.modes.COWBOY)
	pass

func become_cow():
	grid.Economical.fakes-=1
	grid.exchange_char(ii,jj,self,load("res://Cow.gd"))


func _ready():

	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
