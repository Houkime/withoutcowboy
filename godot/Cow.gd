extends "res://Character.gd"

var milktw
const MILKANITIME=0.3
var targetbucket

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
class_name Cow

func _init():
	randomize()
	if (randi()%2==0):
		CurrentLook="CowA"
	else:
		CurrentLook="CowB"
	ButtInfo=[ {
		texture="res://sprites/Bucket_full.0001.png",
		action="milk"
		},
		{
		texture="res://sprites/CowBody2.0001.png",
		action="stop_milking"
		},
		{
		texture="res://sprites/CowboyHat.0001.png",
		action="become_fake"
		}
		]
# Called when the node enters the scene tree for the first time.

func start_milking():
	milktw.start()
	special=true
	pass
func stop_milking():
	if (targetbucket!=null):
		targetbucket.has_cow=false
		targetbucket=null
		milktw.stop_all()
		special=false
	else:
		create_blob("already free")

func milk():
	var vacantbuckets=Array()
	
	for b in grid.buckets:
		if (!b.has_cow):
			vacantbuckets.append(b)
	if (vacantbuckets.size()!=0):
		special=true
		targetbucket = vacantbuckets[randi()%vacantbuckets.size()]
		queue_move(targetbucket.ii,targetbucket.jj)
		move("reached_bucket")
	else:
		create_blob("no vacant buckets, bro!")

func reached_bucket():
	targetbucket.has_cow=true
	start_milking()
	
func _ready():
	milktw=Tween.new()
	grid.cows.append(self)
	var organ = $"VisualChar/Body/Organ"
	milktw.interpolate_property(organ,"scale",organ.scale, Vector2(1,0.5),MILKANITIME,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
	add_child(milktw)
	milktw.repeat=true
	
	pass # Replace with function body.

func cow_deregister():
	grid.cows.remove(grid.cows.find(self))

func become_fake():
	if (grid.Economical.fakes==0):
		grid.Economical.fakes+=1
		if (targetbucket!=0):
			stop_milking()
		cow_deregister()
		grid.exchange_char(ii,jj,self,FakeCowboy)
		pass
	else:
		create_blob("no more hats, bro!")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
