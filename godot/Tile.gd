extends Node2D

var occupied=false
var i
var j
onready var grid=$".."

func activate():
	deactivate()
	if (!occupied):
		$Vacant.visible=true
	else:
		$Occupied.visible=true
		
func deactivate():
	for c in get_children():
		c.visible=false

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Vacant_button_down():
	var cr = grid.spawn_building(i,j,Cross)
	cr.FutureBuildingClass=grid.buildtarget
	grid.buildtarget=null
	pass # Replace with function body.
